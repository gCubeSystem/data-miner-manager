This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

# Changelog for "data-miner-manager"



## [v1.14.0]

 - Updated maven-portal-bom to 4.0.0




## [v1.13.0]

 - Added support to default value for ListParameter [#24026]



## [v1.12.0] - 2021-10-06

 - Added cluster description in Service Info [#19213]



## [v1.11.0] - 2019-06-14

- Added service info [#12594]
- Added support to show log information [#11711]
- Added support to show files html, json, pdf, txt [#17106]
- Updated information show to the user when a computation is submitted [#17030]
- Added Item Id support [#16503]



## [v1.10.0] - 2019-04-01

- Added location and zoom support [#11708]
- Added coordinates EPSG:4326 and EPSG:3857 support [ticket #11710]



## [v1.9.1] - 2018-12-13

- Updated to support StorageHub properties [#11720]



## [v1.9.0] - 2018-10-01

- Updated download item to support StorageHub[#11720]



## [v1.8.0] - 2018-07-01

- Updated to new WorkspaceExplorer widget that support StorageHub[#11721]



## [v1.7.0] - 2018-06-01

- Integrated DataMiner CL for simplify integration with new StorageHub[#11720]
- Added refresh button in operators panel[#11741]
- Added hyperlink for log that contains http reference[ticket #11529]



## [v1.6.0] - 2016-11-09

- Added NetCDF files support
- Added [TEXTAREA] string support



## [v1.5.0] - 2016-06-12

- Support Java 8 compatibility [#8471]



## [v1.4.0] - 2016-02-15

- Fixed load balancing [#7576]



## [v1.3.0] - 2016-02-15

- Updated PortalContext [#6278]
- Added encoded parameters in equivalent http request [ticket #7167]



## [v1.2.0] - 2016-12-01

- Updated Output support
- Added PortalContext



## [v1.1.0] - 2016-10-01

- Updated to Auth 2.0
- Added Spatial data support [#4172]
- Added Temporal data support [#4172]



## [v1.0.0] - 2016-07-01

- First release


